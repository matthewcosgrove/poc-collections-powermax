#!/bin/bash

set -eu

python3 -m venv .venv
source .venv/bin/activate
pip install -U setuptools pip 'molecule[docker]'
molecule --version
molecule init scenario

