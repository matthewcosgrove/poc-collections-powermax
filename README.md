PoC to demonstrate Ansible collections usage as a way to import modules

This follows the best practises for using collections as discussed in https://www.jeffgeerling.com/blog/2020/ansible-best-practices-using-project-local-collections-and-roles

Specifically, avoiding the use of global collections and roles on the local workstation via [ansible.cfg](ansible.cfg) so that the collection gets installed in the project root. Here we have added `ansible_collections` to [.gitignore](.gitignore). NOTE: `ansible_collections` is a strict path naming convention. See https://docs.ansible.com/ansible/latest/user_guide/collections_using.html#installing-collections-with-ansible-galaxy which states "The install command automatically appends the path ansible_collections to the one specified with the -p option unless the parent directory is already in a folder called ansible_collections"

The steps to get this project set up and to run the molecule tests are

```
ansible-galaxy collection install -r requirements.yml
python3 -m venv .venv
source .venv/bin/activate
pip install -U setuptools pip 'molecule[docker]'
molecule --version
molecule --debug test
```

There was a one time set up for molecule for testing the playbook with

```
molecule init scenario
```

There was a gotchya with Molecule, which needs special configuration to find the collection.

Running the playbook is fine by itself

`ansible-playbook -vvvv playbook.yml`

but Molecule can't find the collection unless you override the env var in [molecule.yml](molecule/default/molecule.yml)

```
provisioner:
  name: ansible
  lint:
    name: ansible-lint
  env:
    ANSIBLE_COLLECTIONS_PATHS: "$MOLECULE_PROJECT_DIRECTORY"
```
